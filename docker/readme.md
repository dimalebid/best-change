### Before First Star

1. Включаем docker
2. В папке ```my_project/docker/php``` создать папку ```logs``` а в ней файл ```access.log``` и ```error.log```
3. из ubuntu запускаем команду ```docker-compose up -d``` из папки ```my_project/docker```
4. в шторме запускаем файл ```docker-compose.yml```
5. из ubuntu выполняем команду: ```sudo chmod 777 -R storage/ bootstrap/ public/```
6. В файле ```.env``` настраиваем подключение к БД. Вот такие данные: ```DB_HOST=db```, ```DB_DATABASE=db_services```, ```DB_USERNAME=root```, ```DB_PASSWORD=root```. Если хочешь подключиться к БД через шторм то указывай HOST ```localhost``` к БД 
7. запускаем команды из php контейнера: ```php artisan key:generate``` ```php artisan optimize``` ```php artisan migrate```
