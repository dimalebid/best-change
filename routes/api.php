<?php

use App\Http\Controllers\CurrenciesController;
use App\Http\Controllers\CurrenciesParseController;
use Illuminate\Support\Facades\Route;

Route::get('parse', [CurrenciesParseController::class, 'parse'])->name('currencies.parse');
Route::middleware('auth:api')->get('courses', [CurrenciesController::class, 'index'])->name('currencies.index');
Route::middleware('auth:api')->get('course/{send_currency}/{receive_currency}', [CurrenciesController::class, 'show'])->name('currencies.show');
