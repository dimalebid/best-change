<?php

namespace App\Http\Controllers;

use App\Models\BestCurrency;
use App\Services\ParseCurrencies;
use Illuminate\Routing\Controller;

class CurrenciesParseController extends Controller
{
    public function parse(ParseCurrencies $service)
    {
        $bestCurrencies = $service->best();

        BestCurrency::truncate();

        foreach ($bestCurrencies as $currency) {
            BestCurrency::create($currency);
        }

        return response('Success!', 200);
    }
}
