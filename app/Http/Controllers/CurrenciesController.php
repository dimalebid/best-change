<?php

namespace App\Http\Controllers;

use App\Http\Requests\CurrenciesRequest;
use App\Http\Resources\CurrencyResource;
use App\Models\BestCurrency;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller;

class CurrenciesController extends Controller
{
    public function index(CurrenciesRequest $request): ResourceCollection
    {
        return CurrencyResource::collection(BestCurrency::filter($request)->get());
    }

    public function show(int $sendCurrency, int $receiveCurrency): CurrencyResource
    {
        return new CurrencyResource(BestCurrency::where([
            'send_currency_id' => $sendCurrency,
            'receive_currency_id' => $receiveCurrency,
        ])->firstOrFail());
    }
}
