<?php

namespace App\Http\Requests;

use App\Services\Contracts\CurrenciesFilterContract;
use Illuminate\Foundation\Http\FormRequest;

class CurrenciesRequest extends FormRequest implements CurrenciesFilterContract
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'send_currency_id'    => ['nullable', 'integer'],
            'receive_currency_id' => ['nullable', 'integer'],
        ];
    }

    public function sendCurrencyId(): ?int
    {
        return $this->get('send_currency_id');
    }

    public function receiveCurrencyId(): ?int
    {
        return $this->get('receive_currency_id');
    }
}
