<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'send_currency_id'    => $this->send_currency_id,
            'receive_currency_id' => $this->receive_currency_id,
            'send'                => $this->send,
            'receive'             => $this->receive,
        ];
    }
}
