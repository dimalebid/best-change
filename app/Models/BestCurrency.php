<?php

namespace App\Models;

use App\Services\Contracts\CurrenciesFilterContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BestCurrency extends Model
{
    use HasFactory;

    protected $fillable = [
        'send_currency_id',
        'receive_currency_id',
        'send',
        'receive'
    ];

    public function scopeFilter($query, CurrenciesFilterContract $filter)
    {
        if ($filter->sendCurrencyId()) {
            $query = $query->where('send_currency_id', $filter->sendCurrencyId());
        }

        if ($filter->receiveCurrencyId()) {
            $query = $query->where('receive_currency_id', $filter->receiveCurrencyId());
        }

        return $query->orderByDesc('created_at');
    }
}
