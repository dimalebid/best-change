<?php

namespace App\Services;

class ParseCurrencies extends Currencies
{
    private array $currencies;

    public function __construct()
    {
        $parseFile = new ParseByUrl('http://api.bestchange.ru/info.zip');
        $this->currencies = $parseFile->getCurrencies();
    }

    public function best(): array
    {
        return $this->bestCurrencies($this->currencies);
    }
}
