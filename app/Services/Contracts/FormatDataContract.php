<?php

namespace App\Services\Contracts;

interface FormatDataContract
{
    public function data(): array;
}
