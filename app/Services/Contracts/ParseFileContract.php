<?php

namespace App\Services\Contracts;

interface ParseFileContract
{
    public function getCurrencies(): array;
}
