<?php

namespace App\Services\Contracts;

interface CurrenciesFilterContract
{
    public function sendCurrencyId():? int;
    public function receiveCurrencyId():? int;
}
