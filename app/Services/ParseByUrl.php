<?php

namespace App\Services;

use App\Services\Contracts\ParseFileContract;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class ParseByUrl implements ParseFileContract
{
    private string $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function getCurrencies(): array
    {
        $filename = 'info.zip';
        $disk     = Storage::disk('local');
        $zip      = new ZipArchive();
        $ratesNew = array();
        $disk->put($filename, file_get_contents($this->url));

        if ($zip->open($disk->path($filename), ZipArchive::CREATE) === TRUE) {
            $rates      = $zip->getFromName('bm_rates.dat');
            $formatData = new FormatDataFromFile($rates);
            $ratesNew   = $formatData->data();
            $zip->close();
        }

        $disk->delete($filename);

        return $ratesNew;
    }
}
