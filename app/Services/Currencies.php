<?php

namespace App\Services;

class Currencies
{
    protected function bestCurrencies(array $currencies): array
    {
        $bestCurrencies = array();

        foreach ($currencies as $currency) {
            if (is_bool($currency[0]['send'])) {
                $this->sort($currency, 'receive', SORT_DESC);
            } else {
                $this->sort($currency, 'send');
            }

            $bestCurrencies[] = $currency[0];
        }

        return $bestCurrencies;
    }

    private function sort(&$array, $column, $sortOrder = SORT_ASC) {
        $sort_col = array();
        foreach ($array as $key=> $row) {
            $sort_col[$key] = $row[$column];
        }

        array_multisort($sort_col, $sortOrder, $array);
    }
}
