<?php

namespace App\Services;

use App\Services\Contracts\FormatDataContract;

class FormatDataFromFile implements FormatDataContract
{
    private string $data;

    public function __construct(string $data)
    {
        $this->data = $data;
    }

    public function data(): array
    {
        $data    = explode("\n", $this->data);
        $dataNew = array();

        foreach ($data as $item) {
            $item = explode(";", $item);
            $dataNew[$item[0] . '_' . $item[1]][] = [
                'send_currency_id'    => $item[0],
                'receive_currency_id' => $item[1],
                'send'                => $item[3],
                'receive'             => $item[4]
            ];
        }

        return $dataNew;
    }
}
