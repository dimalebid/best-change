<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBestCurrenciesTable extends Migration
{
    public function up()
    {
        Schema::create('best_currencies', function (Blueprint $table) {
            $table->id();
            $table->integer('send_currency_id')->index();
            $table->integer('receive_currency_id')->index();
            $table->string('send');
            $table->string('receive');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('best_currencies');
    }
}
